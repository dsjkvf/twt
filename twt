#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# HEADER

# Details
__title__      = ""
__author__     = "dsjkvf"
__maintainer__ = "dsjkvf"
__email__      = "dsjkvf@gmail.com"
__copyright__  = "dsjkvf"
__credits__    = []
__license__    = "GPL"
__version__    = "1.0.1"
__status__     = "Production"

# MODULES

# System

import sys
import os
import tweepy
from datetime import datetime, timedelta, timezone
import textwrap
import re

# GLOBALS

# Preferences
CREDS = 'keychain'
CREDS = 'gpg'
DELTA = 24
COUNT = 100

# HELPERS

# Format the text of the tweet
def format_text(s, type):
    if type == 'user':
        return "\033[1;33m" + s + ":\033[0m"
    elif type == 'date':
        return "\033[1;32m" + s + "\033[0m"
    elif type == 'link':
        return "\033[38;05;242m\033[3m" + s + "\033[23m\033[0m"
    elif type == 'text':
        s = s.replace('\n', ' ')
        s = s.replace('&amp;', '&')
        s = re.sub(r'(\$[A-Z]+)', r'\033[1;31m\1\033[0m', s)
        s = re.sub(r'(http[^ ]+)', r'\033[1;34m\1\033[0m', s)
        s = re.sub(r'(@[^ ]+)', r'\033[38;05;246m\1\033[0m', s)
        s = re.sub(r'(#[^ ]+)', r'\033[1;37m\1\033[0m', s)
        s = re.sub("\s\s+", " ", s)
        s = textwrap.fill(s, COLS, break_long_words = False, break_on_hyphens = False)
        return s

# Format the included URLs of the tweet
def format_urls(t):
    urls = t.entities['urls']
    if len(urls) > 0:
        text = ''
        texts = re.split(r'https://t.co/[A-Za-z0-9]+', t.full_text)
        for i in range(len(texts)):
            try:
                text = text + texts[i] + str(urls[i]['expanded_url'])
            except IndexError:
                text = text + texts[i]
    else:
        text = t.full_text
    return text

# Format the attached media of the tweet
def format_media(t):
    try:
        return t.entities['media'][0]['media_url_https']
    except KeyError:
        return ''

# Format the tweet based on the helpers defined above
def format(t):
    print(format_text(str(t.created_at.astimezone(tz = tz))[:19], 'date') + ' ' + format_text(str(t.user.name), 'user'))
    print(format_text('https://twitter.com/' + str(t.user.screen_name) + '/status/' + str(t.id), 'link'))
    try:
        print(format_text('RT @' + str(t.retweeted_status.author.screen_name), 'text') + ':\n' + format_text(format_urls(t.retweeted_status), 'text'))
        pic = format_media(t.retweeted_status)
    except AttributeError:
        print(format_text(format_urls(tweet), 'text'))
        pic = format_media(tweet)
    try:
        tmux = os.environ["TMUX"]
    except KeyError:
        tmux = ''
    if pic != '':
        if tmux == '' and not os.system('which viu >/dev/null 2>&1'):
            os.system('curl -s ' + pic + ' | viu -')
        else:
            print(format_text(pic, 'text'))

# MAIN

# check the terminal
try:
    COLS = os.get_terminal_size()[0]
except OSError:
    COLS = 85
if COLS < 15:
    print("ERROR: the terminal is too small")
    sys.exit(1)

# check the command line options
if len(sys.argv) > 1 and sys.argv[1] not in ['-c', '-h', '-l',  '-d', '-D', '-p', '-u']:
    print("USAGE:")
    print("      twt")
    print("      twt -u <USERNAME> [<USERNAME> ... ]")
    print("      twt -p <TEXT TO POST>")
    sys.exit(1)

# check the credentials
if os.path.isfile(os.path.abspath(__file__) + '.gpg') and CREDS != 'keychain':
    try:
        try:
            import gnupg
        except ModuleNotFoundError:
            print("ERROR: python-gnupg module is not installed")
            sys.exit(1)
        with open(os.path.abspath(__file__) + '.gpg', 'rb') as gpgfile:
            consumer_key, consumer_secret, access_token, access_secret, bearer_token = str(gnupg.GPG().decrypt_file(gpgfile)).split()
    except ValueError:
        print("ERROR: can't read credentials")
        sys.exit(1)
elif sys.platform == 'darwin':
    def keyring(service, login):
            cmd = os.popen('/usr/bin/security find-generic-password -w -s ' + service + ' -a ' + login)
            rez = cmd.read()[:-1]
            if cmd.close() is None:
                return rez
            else:
                print("ERROR: can't read credentials")
                sys.exit(1)
    consumer_key    = keyring('twitter', 'consumer_key')
    consumer_secret = keyring('twitter', 'consumer_secret')
    access_token    = keyring('twitter', 'access_token')
    access_secret   = keyring('twitter', 'access_secret')
else:
    print("ERROR: can't read credentials")
    sys.exit(1)

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
api = tweepy.API(auth)
client = tweepy.Client(bearer_token = bearer_token)

# check for the COUNT option, takes the number of tweets to display as a parameter
for i in range(len(sys.argv)):
    if sys.argv[i] == '-c':
        try:
            COUNT = int(sys.argv[i+1])
            del sys.argv[i]
            del sys.argv[i]
            break
        # if the count option '-c' isn't followed by a number, just skip it
        except ValueError:
            del sys.argv[i]

# check for the DELTA option, takes a number of hours to parse as a parameter
for i in range(len(sys.argv)):
    if sys.argv[i] == '-h':
        try:
            DELTA = int(sys.argv[i+1])
            del sys.argv[i]
            del sys.argv[i]
            break
        # if the delta option '-h' isn't followed by a number, just skip it
        except ValueError:
            del sys.argv[i]
# detect the current timezone
tz = round((datetime.now() - datetime.utcnow()).total_seconds())
tz = timezone(timedelta(seconds = tz))
# calculate the upper and the lower time limits
now = datetime.now().astimezone(tz = tz)
prv = timedelta(hours = DELTA)

# check the remaining options
if len(sys.argv) == 1:
    # the home timeline mode, no parameters needed
    tweets = api.home_timeline(count = COUNT, tweet_mode = 'extended')
    for tweet in tweets:
        if now - prv <= tweet.created_at.astimezone(tz = tz) <= now:
            format(tweet)
elif sys.argv[1] == '-l':
    tweets = client.get_list_members(id = sys.argv[2])
    for tweet in tweets:
        if now - prv <= tweet.created_at.astimezone(tz = tz) <= now:
            format(tweet)
elif sys.argv[1] == '-d':
    # the debug mode, takes the tweet's ID or URL as a parameter
    if len(sys.argv) > 2:
        id = sys.argv[2]
        if 'http' in id:
            id = [i for i in id.split('/') if i][-1]
        import pprint
        pprint.pprint(api.get_status(id)._json)
    else:
        print("ERROR: the tweet's ID or URL is missing")
elif sys.argv[1] == '-D':
    api.destroy_status(sys.argv[2])
elif sys.argv[1] == '-p':
    # the post mode, takes the tweet as a parameter
    if len(sys.argv) > 2:
        api.update_status(' '.join(sys.argv[2:]))
    else:
        print("ERROR: the tweet is missing")
elif sys.argv[1] == '-u':
    # the user(s) timeline mode, takes the screen name(s) as a parameter(s)
    if len(sys.argv) > 2:
        for user in sys.argv[2:]:
            print('\n' + format_text(user, 'user'))
            tweets = api.user_timeline(screen_name = user, count = COUNT, tweet_mode = 'extended')
            for tweet in tweets:
                if now - prv <= tweet.created_at.astimezone(tz = tz) <= now:
                    format(tweet)
    else:
        print("ERROR: the screen name is missing")
