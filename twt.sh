#!/usr/bin/env bash

case $1 in
    ""|post)
        vi +'setf twt' /tmp/twt && $HOME/tmp/devel/twt/twt -p "$(cat /tmp/twt)" || printf "\e[0;31mAborted!\e[0m\n"
        ;;
    -d|-D|del|delete)
        ID=$2
        ID=${ID##*status\/}
        ID=${ID%%[^0-9]*}
        $HOME/tmp/devel/twt/twt -D $ID
        ;;
    -v|-s|view|stream)
        $HOME/tmp/devel/twt/twt
        ;;
    -u|user)
        shift
        $HOME/tmp/devel/twt/twt -u $@
        ;;
esac

