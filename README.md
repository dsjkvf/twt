twt
===

## About

`twt` is a simple CLI Twitter client, which allows you to post tweets, read your and your friends' timeline, or read the timeline of some specified users. It can also display pictures if [`viu`](https://github.com/atanunq/viu) is available and `twt` is not running in `tmux`

![](https://i.imgur.com/toXfM6P.png)


## Configuration

In order to function, `twt` needs [Twitter API credentials](https://developer.twitter.com/en/docs/authentication/oauth-1-0a/obtaining-user-access-tokens). Which can be either stored in a `twt.gpg` file alongside the main script, or in the Keychain on macOS (see the code to get the details).

Other options, like the time span for the latest tweets (24h by default), or the count of the tweets to parse (100 by the default) can be tweaked with `-h <NUMBER OF HOURS>` and `-c <NUMBER OF TWEETS>` command line parameters.


## Usage

  - Read your home timeline:

    `twt`

  - Post a new tweet:

    `twt -p <TEXT TO BE POSTED>`

  - Read the timelines of other users:

    `twt -u <USERNAME1> [<USERNAME2> ... ]`

  - Read the short help:

    `twt help`
